package com.ioasys.movies.security;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ioasys.movies.models.Usuario;
import com.ioasys.movies.services.usuario.UsuarioService;
import com.ioasys.movies.utility.Md5Utils;

@Service
public class MyUserDetails implements UserDetailsService {
	
	private UsuarioService usuarioService;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		final Usuario usuario = usuarioService.obterUsuarioPorLogin(login);
		
		if (usuario == null) {
			throw new UsernameNotFoundException("Usuário '" + login + "' não encontrado");
		}

		UserDetails userDetails = User.withUsername(login)
							.password(Md5Utils.getMd5(usuario.getSenha()))
							.accountExpired(false)
							.accountLocked(false)
							.credentialsExpired(false)
							.disabled(false)
							.build();
		
		return userDetails; 
	}
	
		
}