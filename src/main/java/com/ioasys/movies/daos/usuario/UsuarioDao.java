package com.ioasys.movies.daos.usuario;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ioasys.movies.models.Usuario;


@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Long> {
	
	/*static EntityManagerFactory factory = Persistence.createEntityManagerFactory("ioasys_movies");
	EntityManager entityManager = factory.createEntityManager();

	default Predicate montarListar(Usuario usuarioFiltro, CriteriaBuilder criteriaBuilder,Root<Usuario> root) {
			
		Predicate predicate = criteriaBuilder.and();
		
		if(usuarioFiltro != null && usuarioFiltro.getNome() != null
				&& !usuarioFiltro.getNome().isEmpty()) {
			predicate = criteriaBuilder.and(predicate, 
					criteriaBuilder.equal(root.get("nome").as(String.class), usuarioFiltro.getNome()));
		}
		
		if(usuarioFiltro != null && usuarioFiltro.getAdministrador() != null) {
			predicate = criteriaBuilder.and(predicate, 
					criteriaBuilder.equal(root.get("administrador").as(Boolean.class), usuarioFiltro.getAdministrador()));
		}
		
		if(usuarioFiltro != null && usuarioFiltro.getAtivo() != null) {
			predicate = criteriaBuilder.and(predicate, 
					criteriaBuilder.equal(root.get("ativo").as(Boolean.class), usuarioFiltro.getAtivo()));
		}

		return predicate;
		
	}

	default int quantidadeTotalListar(Usuario usuarioFiltro) {
		int quantidadeTotalRegistros = 0;
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
		Root<Usuario> root = countQuery.from(Usuario.class);
		Predicate predicate = montarListar(usuarioFiltro , criteriaBuilder , root);
		countQuery.select(criteriaBuilder.count(root));
		countQuery.where(predicate);
		quantidadeTotalRegistros = entityManager.createQuery(countQuery).getSingleResult().intValue();
		return quantidadeTotalRegistros;
	}

	default List<Usuario> listar(UsuarioFiltroDto usuarioFiltroDto) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteriaQuery =  criteriaBuilder.createQuery(Usuario.class);
		Root<Usuario> root = criteriaQuery.from(Usuario.class);
		Predicate predicate = montarListar(usuarioFiltroDto.getUsuario() , criteriaBuilder , root);
		criteriaQuery.where(predicate);
		criteriaQuery.orderBy(criteriaBuilder.asc(root.get("nome")));
		TypedQuery<Usuario> typedQuery = entityManager.createQuery(criteriaQuery);
		if(usuarioFiltroDto.getPrimeiroRegistro() != null && usuarioFiltroDto.getTamanhoPagina() != null) {
			typedQuery.setFirstResult(usuarioFiltroDto.getPrimeiroRegistro());
			typedQuery.setMaxResults(usuarioFiltroDto.getTamanhoPagina());
		}
		return typedQuery.getResultList();
	}*/
	
	@Query("SELECT usuario FROM Usuario usuario WHERE usuario.administrador = FALSE AND usuario.ativo = TRUE ORDER BY nome ")
	public List<Usuario> listarUsuarioComumAtivo();
	
	@Query("SELECT usuario FROM Usuario usuario WHERE usuario.login = :login")
	public Usuario obterUsuarioPorLogin(@Param("login") String login);
	
}
