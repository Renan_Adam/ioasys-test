package com.ioasys.movies.daos.filme;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ioasys.movies.models.Filme;
import com.ioasys.movies.models.dto.FilmeFiltroDto;


@Repository
public interface FilmeDao extends JpaRepository<Filme, Long> {
	
	/*static EntityManagerFactory factory = Persistence.createEntityManagerFactory("ioasys_movies");
	EntityManager entityManager = factory.createEntityManager();
	
	default Predicate montarListar(FilmeFiltroDto filmeFiltroDto, CriteriaBuilder criteriaBuilder,Root<Filme> root) {
			
		Predicate predicate = criteriaBuilder.and();
		
		if(filmeFiltroDto != null && filmeFiltroDto.getNome() != null
				&& !filmeFiltroDto.getNome().isEmpty()) {
			predicate = criteriaBuilder.and(predicate, 
					criteriaBuilder.equal(root.get("nome").as(String.class), filmeFiltroDto.getNome()));
		}
		
		if(filmeFiltroDto != null && filmeFiltroDto.getNomeDiretor() != null
				&& !filmeFiltroDto.getNomeDiretor().isEmpty()) {
			predicate = criteriaBuilder.and(predicate, 
					criteriaBuilder.equal(root.join("elencos").get("nome").as(String.class), filmeFiltroDto.getNomeDiretor()));
		}
		
		if(filmeFiltroDto != null && filmeFiltroDto.getNomeAtor() != null
				&& !filmeFiltroDto.getNomeAtor().isEmpty()) {
			predicate = criteriaBuilder.and(predicate, 
					criteriaBuilder.equal(root.join("elencos").get("nome").as(String.class), filmeFiltroDto.getNomeAtor()));
		}
		
		if(filmeFiltroDto != null && filmeFiltroDto.getNomeGenero() != null
				&& !filmeFiltroDto.getNomeGenero().isEmpty()) {
			predicate = criteriaBuilder.and(predicate, 
					criteriaBuilder.equal(root.join("generos").get("nome").as(String.class), filmeFiltroDto.getNomeGenero()));
		}

		return predicate;
		
	}

	default int quantidadeTotalListar(FilmeFiltroDto filmeFiltroDto) {
		int quantidadeTotalRegistros = 0;
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
		Root<Filme> root = countQuery.from(Filme.class);
		Predicate predicate = montarListar(filmeFiltroDto , criteriaBuilder , root);
		countQuery.select(criteriaBuilder.count(root));
		countQuery.where(predicate);
		quantidadeTotalRegistros = entityManager.createQuery(countQuery).getSingleResult().intValue();
		return quantidadeTotalRegistros;
	}

	default List<Filme> listar(FilmeFiltroDto filmeFiltroDto) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Filme> criteriaQuery =  criteriaBuilder.createQuery(Filme.class);
		Root<Filme> root = criteriaQuery.from(Filme.class);
		Predicate predicate = montarListar(filmeFiltroDto , criteriaBuilder , root);
		criteriaQuery.where(predicate);
		criteriaQuery.orderBy(criteriaBuilder.asc(root.get("nome")));
		TypedQuery<Filme> typedQuery = entityManager.createQuery(criteriaQuery);
		if(filmeFiltroDto.getPrimeiroRegistro() != null && filmeFiltroDto.getTamanhoPagina() != null) {
			typedQuery.setFirstResult(filmeFiltroDto.getPrimeiroRegistro());
			typedQuery.setMaxResults(filmeFiltroDto.getTamanhoPagina());
		}
		return typedQuery.getResultList();
	}*/
	
	@Query("SELECT filme FROM Filme filme "
			+ " JOIN filme.elencos elenco "
			+ " JOIN filme.generos genero "
			+ " WHERE 1 = 1 "
			+ " AND (:#{#filtro.nome} IS NULL OR filme.nome like '%' || :#{#filtro.nome ?: ''} || '%') "
			+ " AND (:#{#filtro.nomeDiretor} IS NULL OR elenco.nome like '%' || :#{#filtro.nomeDiretor ?: ''} || '%') "
			+ " AND (:#{#filtro.nomeAtor} IS NULL OR elenco.nome like '%' || :#{#filtro.nomeAtor ?: ''} || '%') "
			+ " AND (:#{#filtro.nomeGenero} IS NULL OR genero.nome like '%' || :#{#filtro.nomeGenero ?: ''} || '%') "
			+ " ORDER BY filme.nome ")
	public List<Filme> listar(FilmeFiltroDto filtro);
	
	@Query("SELECT filme FROM Filme filme WHERE filme.id = :id")
	public Filme obter(@Param("id") Long id);
	
}
