package com.ioasys.movies.daos.filme;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ioasys.movies.models.Votacao;


@Repository
public interface VotacaoDao extends JpaRepository<Votacao, Long> {
	
	@Query("SELECT AVG(votacao.nota) FROM Votacao votacao WHERE votacao.filme.id = :id")
	public Integer obterMediaVoto(@Param("id") Long id);
	
}
