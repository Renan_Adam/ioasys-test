package com.ioasys.movies.services.filme;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ioasys.movies.models.dto.FilmeDto;
import com.ioasys.movies.daos.filme.FilmeDao;
import com.ioasys.movies.daos.filme.VotacaoDao;
import com.ioasys.movies.models.Filme;
import com.ioasys.movies.models.Votacao;
import com.ioasys.movies.models.dto.FilmeFiltroDto;

@Service
public class FilmeServiceImpl implements FilmeService {

	@Autowired
	private FilmeDao filmeDao;

	@Autowired
	private VotacaoDao votacaoDao;
	
	@Override
	public Filme salvar(FilmeDto filmeDto) {
		Filme filme = filmeDto.getFilme();
		if(filmeDto != null && filmeDto.getUsuario() != null 
				&& filmeDto.getUsuario().getAdministrador() && filmeDto.getUsuario().getAtivo()) {
			return filmeDao.saveAndFlush(filme);
		}else {
			return null;
		}
	}	
	
	@Override
	public List<Filme> listar(FilmeFiltroDto filtroFilme) {
		return filmeDao.listar(filtroFilme);
	}

	@Override
	public Votacao votar(Votacao votacao) {
		if(votacao != null && votacao.getNota() <= 4) {
			return votacaoDao.saveAndFlush(votacao);
		} else {
			return null;
		}
	}

	@Override
	public Filme obter(Long id) {
		Filme filme = filmeDao.obter(id);
		filme.setMediaVotacao(votacaoDao.obterMediaVoto(id));
		return filme;
	}
	
}
