package com.ioasys.movies.services.filme;

import java.util.List;

import com.ioasys.movies.models.dto.FilmeDto;
import com.ioasys.movies.models.Filme;
import com.ioasys.movies.models.Votacao;
import com.ioasys.movies.models.dto.FilmeFiltroDto;

public interface FilmeService {
	
	Filme salvar(FilmeDto filmeDto);
	
	Filme obter(Long id);
	
	List<Filme> listar(FilmeFiltroDto filtroFilme);
	
	Votacao votar(Votacao votacao);

}
