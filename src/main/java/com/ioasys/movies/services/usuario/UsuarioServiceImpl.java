package com.ioasys.movies.services.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ioasys.movies.daos.usuario.UsuarioDao;
import com.ioasys.movies.models.Usuario;
import com.ioasys.movies.models.dto.UsuarioFiltroDto;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioDao dao;

	@Override
	public Usuario salvar(Usuario usuario) {
		return dao.saveAndFlush(usuario);
	}	
	
	@Override
	public void deletar(Usuario usuario) {
		usuario.setAtivo(false);
		dao.saveAndFlush(usuario);
	}

	@Override
	public List<Usuario> listarUsuarioComumAtivo(UsuarioFiltroDto usuarioFiltroDto){
		if(usuarioFiltroDto != null && usuarioFiltroDto.getUsuario() != null 
				&& usuarioFiltroDto.getUsuario().getAdministrador()
				&& usuarioFiltroDto.getUsuario().getAtivo()) {
			return dao.listarUsuarioComumAtivo();
		} else {
			return null;
		}
	}

	@Override
	public Usuario obterUsuarioPorLogin(String login) {
		return dao.obterUsuarioPorLogin(login);
	}
	
}
