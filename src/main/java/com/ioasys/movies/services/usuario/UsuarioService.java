package com.ioasys.movies.services.usuario;

import java.util.List;

import com.ioasys.movies.models.Usuario;
import com.ioasys.movies.models.dto.UsuarioFiltroDto;

public interface UsuarioService {
	
	Usuario salvar(Usuario usuario);
	
	void deletar(Usuario usuario);
	
	List<Usuario> listarUsuarioComumAtivo(UsuarioFiltroDto usuarioFiltroDto);
	
	Usuario obterUsuarioPorLogin(String login);

}
