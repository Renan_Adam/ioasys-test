package com.ioasys.movies.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ioasys.movies.models.Filme;
import com.ioasys.movies.models.Votacao;
import com.ioasys.movies.models.dto.FilmeDto;
import com.ioasys.movies.models.dto.FilmeFiltroDto;
import com.ioasys.movies.services.filme.FilmeService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins="*")
@RequestMapping(value = "/api/filme")
@Api(value = "API REST Filme")
public class FilmeController {
	
	@Autowired
	private FilmeService filmeService;

	@PostMapping("/salvar")
	public Filme salvar(@RequestBody FilmeDto filmeDto) {
		try {
			return filmeService.salvar(filmeDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@PostMapping("/listar")
	public List<Filme> listar(@RequestBody FilmeFiltroDto filmeFiltroDto) {
		try {
			return filmeService.listar(filmeFiltroDto);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Filme>();
		}
	}

	@GetMapping("/obter/{id}")
	public Filme obter(@PathVariable("id") Long id) {
		try {
			return filmeService.obter(id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@PostMapping("/votar")
	public Votacao votar(@RequestBody Votacao votacao) {
		try {
			return filmeService.votar(votacao);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}