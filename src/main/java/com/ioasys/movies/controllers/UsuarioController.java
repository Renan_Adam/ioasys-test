package com.ioasys.movies.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ioasys.movies.models.Usuario;
import com.ioasys.movies.models.dto.UsuarioFiltroDto;
import com.ioasys.movies.services.usuario.UsuarioService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins="*")
@RequestMapping(value = "/api/usuario")
@Api(value = "API REST Usuário")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;

	@PostMapping("/salvar")
	public Usuario salvar(@RequestBody Usuario usuario) {
		try {
			return usuarioService.salvar(usuario);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@PostMapping("/deletar")
	public void deletar(@RequestBody Usuario usuario) {
		try {
			usuarioService.deletar(usuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@PostMapping("/listarcomumativo")
	public List<Usuario> listarTodos(@RequestBody UsuarioFiltroDto usuarioFiltroDto) {
		try {
			return usuarioService.listarUsuarioComumAtivo(usuarioFiltroDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}