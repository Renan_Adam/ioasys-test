package com.ioasys.movies.models;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Entity
@Table(name = "VOTACAO", catalog = ModelsConst.DATABASE, schema = ModelsConst.SCHEMA)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Votacao implements Serializable {
	
	private static final long serialVersionUID = -1559815983375919889L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@ApiModelProperty(value = "Filme votado pelo usuário")
	@ManyToOne
    @JoinColumn(name = "id_filme", nullable = false)	
    private Filme filme;
    
	@ApiModelProperty(value = "Usuário que votou no filme")
	@ManyToOne
    @JoinColumn(name = "id_usuario", nullable = false)
    private Usuario usuario;
    
	@ApiModelProperty(value = "Nota do usuário no filme")
    @Column(name = "nota", nullable = false)
    private Integer nota;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Filme getFilme() {
		return filme;
	}

	public void setFilme(Filme filme) {
		this.filme = filme;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Votacao other = (Votacao) obj;
		return Objects.equals(id, other.id);
	}
    
}
