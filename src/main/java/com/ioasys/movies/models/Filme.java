package com.ioasys.movies.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Entity
@Table(name = "FILME", catalog = ModelsConst.DATABASE, schema = ModelsConst.SCHEMA)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Filme implements Serializable {

	private static final long serialVersionUID = -8637965073532652554L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@ApiModelProperty(value = "Nome do filme")
    @Column(name = "nome", nullable = false)
    private String nome;
    
	@ApiModelProperty(value = "Ano do filme")
    @Column(name = "ano", nullable = false)
    private Integer ano;
    
	@ApiModelProperty(value = "Sinopse do filme")
    @Column(name = "sinopse", nullable = false)
    private String sinopse;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "filme_elenco",
		joinColumns = @JoinColumn(name = "id_filme"),
		inverseJoinColumns = @JoinColumn(name = "id_elenco")
	)
	private Set<Elenco> elencos = new HashSet<Elenco>();

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "filme_genero",
		joinColumns = @JoinColumn(name = "id_filme"),
		inverseJoinColumns = @JoinColumn(name = "id_genero")
	)
	private Set<Genero> generos = new HashSet<Genero>();
	
	@Transient
	private Integer mediaVotacao;
	
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public Set<Elenco> getElencos() {
		return elencos;
	}

	public void setElencos(Set<Elenco> elencos) {
		this.elencos = elencos;
	}

	public Set<Genero> getGeneros() {
		return generos;
	}

	public void setGeneros(Set<Genero> generos) {
		this.generos = generos;
	}

	public Integer getMediaVotacao() {
		return mediaVotacao;
	}

	public void setMediaVotacao(Integer mediaVotacao) {
		this.mediaVotacao = mediaVotacao;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Filme other = (Filme) obj;
		return Objects.equals(id, other.id);
	}
    
}
