package com.ioasys.movies.models;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Entity
@Table(name = "USUARIO", catalog = ModelsConst.DATABASE, schema = ModelsConst.SCHEMA)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario implements Serializable {
	
	private static final long serialVersionUID = -5127253721530377559L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@ApiModelProperty(value = "Nome do usuário")
    @Column(name = "nome", nullable = false)
    private String nome;
    
	@ApiModelProperty(value = "Login do usuário")
    @Column(name = "login", nullable = false)
    private String login;
    
	@ApiModelProperty(value = "Senha do usuário")
    @Column(name = "senha", nullable = false)
    private String senha;
    
	@ApiModelProperty(value = "Indica se o usuário é administrador")
	@Column(name = "administrador", nullable = false)
    private Boolean administrador;
    
	@ApiModelProperty(value = "Indica se o usuário está ativo")
	@Column(name = "ativo", nullable = false)
    private Boolean ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Boolean administrador) {
		this.administrador = administrador;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		return Objects.equals(id, other.id);
	}
    
}
