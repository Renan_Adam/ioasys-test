package com.ioasys.movies.models;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Entity
@Table(name = "ELENCO", catalog = ModelsConst.DATABASE, schema = ModelsConst.SCHEMA)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Elenco implements Serializable {
	
	private static final long serialVersionUID = 5992028637235479150L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@ApiModelProperty(value = "Nome do elenco")
    @ManyToOne
    @JoinColumn(name = "id_tipo_elenco", nullable = false)	
	private TipoElenco tipoElenco;
	
	@ApiModelProperty(value = "Nome do elenco")
    @Column(name = "nome", nullable = false)
    private String nome;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoElenco getTipoElenco() {
		return tipoElenco;
	}

	public void setTipoElenco(TipoElenco tipoElenco) {
		this.tipoElenco = tipoElenco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Elenco other = (Elenco) obj;
		return Objects.equals(id, other.id);
	}
    
}
