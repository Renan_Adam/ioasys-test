package com.ioasys.movies.models.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ioasys.movies.models.Usuario;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioFiltroDto implements Serializable {
	
	private static final long serialVersionUID = -179012968079135842L;

	@ApiModelProperty(value = "Parâmetro de referência do primeiro registro da busca de usuário com paginação")
    private Integer primeiroRegistro;
    
	@ApiModelProperty(value = "Parâmetro de referência do tamanho da página da busca de usuário com paginação")
    private Integer tamanhoPagina;
	
	@ApiModelProperty(value = "Entidade com os campos de filtros da busca de usuários")
    private Usuario usuario;

	
	public Integer getPrimeiroRegistro() {
		return primeiroRegistro;
	}

	public void setPrimeiroRegistro(Integer primeiroRegistro) {
		this.primeiroRegistro = primeiroRegistro;
	}

	public Integer getTamanhoPagina() {
		return tamanhoPagina;
	}

	public void setTamanhoPagina(Integer tamanhoPagina) {
		this.tamanhoPagina = tamanhoPagina;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
    
}
