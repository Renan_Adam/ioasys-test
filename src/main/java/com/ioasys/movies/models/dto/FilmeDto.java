package com.ioasys.movies.models.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ioasys.movies.models.Filme;
import com.ioasys.movies.models.Usuario;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilmeDto implements Serializable {
	
	private static final long serialVersionUID = -179012968079135842L;

	@ApiModelProperty(value = "Dados do filme a ser salvo.")
    private Filme filme;
    
	@ApiModelProperty(value = "Dados do usuário a salvar o filme.")
    private Usuario usuario;
	
	
	public Filme getFilme() {
		return filme;
	}

	public void setFilme(Filme filme) {
		this.filme = filme;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
