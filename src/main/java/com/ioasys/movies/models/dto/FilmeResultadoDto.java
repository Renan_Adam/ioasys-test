package com.ioasys.movies.models.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ioasys.movies.models.Filme;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilmeResultadoDto implements Serializable {
	
	private static final long serialVersionUID = -179012968079135842L;

	@ApiModelProperty(value = "Dados do filme pesquisado.")
    private Filme filme;
    
	@ApiModelProperty(value = "Média de votos do filme pesquisado.")
    private BigDecimal mediaVoto;
	
	public FilmeResultadoDto(Filme filme) {
		super();
		this.filme = filme;
	}

	public FilmeResultadoDto(Filme filme, BigDecimal mediaVoto) {
		super();
		this.filme = filme;
		this.mediaVoto = mediaVoto;
	}

	public Filme getFilme() {
		return filme;
	}

	public void setFilme(Filme filme) {
		this.filme = filme;
	}

	public BigDecimal getMediaVoto() {
		return mediaVoto;
	}

	public void setMediaVoto(BigDecimal mediaVoto) {
		this.mediaVoto = mediaVoto;
	}
	
}
