package com.ioasys.movies.models.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilmeFiltroDto implements Serializable {
	
	private static final long serialVersionUID = -179012968079135842L;

	@ApiModelProperty(value = "Parâmetro de referência do primeiro registro da busca de filme com paginação")
    private Integer primeiroRegistro;
    
	@ApiModelProperty(value = "Parâmetro de referência do tamanho da página da busca de filme com paginação")
    private Integer tamanhoPagina;
	
	@ApiModelProperty(value = "Campo de filtro do nome do filme da busca de filmes")
    private String nome;
	
	@ApiModelProperty(value = "Campo de filtro do nome do diretor da busca de filmes")
    private String nomeDiretor;

	@ApiModelProperty(value = "Campo de filtro do nome do ator da busca de filmes")
    private String nomeAtor;

	@ApiModelProperty(value = "Campo de filtro do nome do gênero da busca de filmes")
    private String nomeGenero;

	
	
	public Integer getPrimeiroRegistro() {
		return primeiroRegistro;
	}

	public void setPrimeiroRegistro(Integer primeiroRegistro) {
		this.primeiroRegistro = primeiroRegistro;
	}

	public Integer getTamanhoPagina() {
		return tamanhoPagina;
	}

	public void setTamanhoPagina(Integer tamanhoPagina) {
		this.tamanhoPagina = tamanhoPagina;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeDiretor() {
		return nomeDiretor;
	}

	public void setNomeDiretor(String nomeDiretor) {
		this.nomeDiretor = nomeDiretor;
	}

	public String getNomeAtor() {
		return nomeAtor;
	}

	public void setNomeAtor(String nomeAtor) {
		this.nomeAtor = nomeAtor;
	}

	public String getNomeGenero() {
		return nomeGenero;
	}

	public void setNomeGenero(String nomeGenero) {
		this.nomeGenero = nomeGenero;
	}
	
}
