CREATE TABLE IF NOT EXISTS public.usuario (
	id serial NOT NULL,
	nome varchar(80) NOT NULL,
	login varchar(20) NOT NULL,
	senha varchar(200) NOT NULL,
	administrador bool NOT NULL DEFAULT false,
	ativo bool NOT NULL DEFAULT true,
	CONSTRAINT pk_usuario PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.genero (
	id serial NOT NULL,
	nome varchar(80) NOT NULL,
	CONSTRAINT pk_genero PRIMARY KEY (id),
	CONSTRAINT uk_genero UNIQUE (nome)
);

CREATE TABLE IF NOT EXISTS public.tipo_elenco (
	id serial NOT NULL,
	nome varchar(80) NOT NULL,
	CONSTRAINT pk_tipo_elenco PRIMARY KEY (id),
	CONSTRAINT uk_tipo_elenco UNIQUE (nome)
);

CREATE TABLE IF NOT EXISTS public.elenco (
	id serial NOT NULL,
	id_tipo_elenco int8 NOT NULL,
	nome varchar(80) NOT NULL,
	CONSTRAINT pk_elenco_filme PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.filme (
	id serial NOT NULL,
	nome varchar(80) NOT NULL,
	ano int4 NOT NULL,
	sinopse text NOT NULL,
	CONSTRAINT pk_filme PRIMARY KEY (id),
	CONSTRAINT uk_filme UNIQUE (nome)
);

CREATE TABLE IF NOT EXISTS public.filme_genero (
	id serial NOT NULL,
	id_filme int8 NOT NULL,
	id_genero int8 NOT NULL,
	CONSTRAINT pk_filme_genero PRIMARY KEY (id),
	CONSTRAINT uk_filme_genero UNIQUE (id_filme,id_genero)
);

CREATE TABLE IF NOT EXISTS public.filme_elenco (
	id serial NOT NULL,
	id_filme int8 NOT NULL,
	id_elenco int8 NOT NULL,
	CONSTRAINT pk_filme_elenco PRIMARY KEY (id),
	CONSTRAINT uk_filme_elenco UNIQUE (id_filme,id_elenco)
);

CREATE TABLE IF NOT EXISTS public.votacao (
	id serial NOT NULL,
	id_filme int8 NOT NULL,
	id_usuario int8 NOT NULL,
	nota int4 NOT NULL,
	CONSTRAINT pk_filme_votacao PRIMARY KEY (id),
	CONSTRAINT uk_filme_votacao UNIQUE (id_filme,id_usuario)
);
