INSERT INTO public.genero (id , nome) VALUES(1, 'COMÉDIA');
INSERT INTO public.genero (id , nome) VALUES(2, 'AÇÃO');
INSERT INTO public.genero (id , nome) VALUES(3, 'AVENTURA');
INSERT INTO public.genero (id , nome) VALUES(4, 'NOVELA');

INSERT INTO public.usuario(id, nome, login, senha, administrador, ativo)VALUES(1, 'RENAN ADAM', 'renan.adam', '123456', false, true);
INSERT INTO public.usuario(id, nome, login, senha, administrador, ativo)VALUES(2, 'RAFAEL', 'rafael', '654321', true, true);
INSERT INTO public.usuario(id, nome, login, senha, administrador, ativo)VALUES(3, 'JOSUE DE CASTRO', 'josue.castro', '125469', false, true);
INSERT INTO public.usuario(id, nome, login, senha, administrador, ativo)VALUES(4, 'MARIA DA SILVA', 'maria.silva', '7842365', false, true);

INSERT INTO public.tipo_elenco (id, nome) VALUES(1,'ATOR');
INSERT INTO public.tipo_elenco (id, nome) VALUES(2,'DIRETOR');
INSERT INTO public.tipo_elenco (id, nome) VALUES(3,'ATOR E DIRETOR');

INSERT INTO public.elenco (id_tipo_elenco, nome) VALUES(1, 'JULIANA PAES');
INSERT INTO public.elenco (id_tipo_elenco, nome) VALUES(2, 'MARCOS PALMEIRAS');
INSERT INTO public.elenco (id_tipo_elenco, nome) VALUES(3, 'DENIS CARVALHO');

INSERT INTO public.filme(id, nome, ano, sinopse)VALUES(1, 'O REI DO GADO', 1998, 'NOVELA BRASILEIRA');
INSERT INTO public.filme(id, nome, ano, sinopse)VALUES(2, 'IRMÃOS CORAGEM', 1994, 'NOVELA BRASILEIRA');

INSERT INTO public.filme_genero (id , id_filme, id_genero) VALUES(1 , 1, 4);
INSERT INTO public.filme_genero (id , id_filme, id_genero) VALUES(2 , 2, 4);

INSERT INTO public.filme_elenco (id, id_filme, id_elenco) VALUES(1, 1, 1);
INSERT INTO public.filme_elenco (id, id_filme, id_elenco) VALUES(2, 1, 2);
INSERT INTO public.filme_elenco (id, id_filme, id_elenco) VALUES(3, 1, 3);

INSERT INTO public.votacao (id, id_filme, id_usuario, nota) VALUES(1, 1, 1, 3);
INSERT INTO public.votacao (id, id_filme, id_usuario, nota) VALUES(2, 1, 2, 4);
