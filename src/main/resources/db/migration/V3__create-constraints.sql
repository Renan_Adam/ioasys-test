ALTER TABLE public.elenco ADD CONSTRAINT fk_tipo_elenco_elenco FOREIGN KEY (id_tipo_elenco) REFERENCES public.tipo_elenco(id);

ALTER TABLE public.filme_genero ADD CONSTRAINT fk_genero_filme_genero FOREIGN KEY (id_genero) REFERENCES public.genero(id);
ALTER TABLE public.filme_genero ADD CONSTRAINT fk_filme_filme_genero FOREIGN KEY (id_filme) REFERENCES public.filme(id);

ALTER TABLE public.filme_elenco ADD CONSTRAINT fk_elenco_filme_elenco FOREIGN KEY (id_elenco) REFERENCES public.elenco(id);
ALTER TABLE public.filme_elenco ADD CONSTRAINT fk_filme_filme_elenco FOREIGN KEY (id_filme) REFERENCES public.filme(id);

ALTER TABLE public.votacao ADD CONSTRAINT fk_filme_votacao FOREIGN KEY (id_filme) REFERENCES public.filme(id);
ALTER TABLE public.votacao ADD CONSTRAINT fk_usuario_votacao FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
